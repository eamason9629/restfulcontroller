package ut.com.geekonablog.restfulcontroller

import com.geekonablog.restfulcontroller.RestfulController
import com.geekonablog.restfulcontroller.controller.BlahController
import com.geekonablog.restfulcontroller.domain.Blah
import com.geekonablog.restfulcontroller.service.BlahService
import groovy.text.GStringTemplateEngine
import groovy.text.Template
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import spock.lang.Ignore
import spock.lang.Specification

import java.lang.reflect.Field
import java.lang.reflect.Method

class RestfulControllerTransformationSpec extends Specification {
    BlahController blahController = new BlahController(blahService: Mock(BlahService))

    @Ignore
    def 'stop point'() {
        setup:
        File physicalFile = new File(new File('').canonicalFile.toString() + '/src/test/groovy/' + BlahController.name.replace('.', '/') + '.groovy')
        final Template template = new GStringTemplateEngine().createTemplate(physicalFile?.text)
        def instance = new GroovyClassLoader().parseClass(template.make([:]).toString()).newInstance()

        expect:
        instance
    }

    def 'has appropriate annotations'() {
        expect:
        BlahController.getAnnotation(RestfulController)
        BlahController.getAnnotation(RestfulController).domain() == Blah
        BlahController.getAnnotation(RestfulController).service() == BlahService
        BlahController.getAnnotation(RestController)
        BlahController.getAnnotation(RequestMapping)
        BlahController.getAnnotation(RequestMapping).value().toList() == ['blah']
        BlahController.methods.find { it.name == 'fetchAll' }.getAnnotation(ResponseBody)
        BlahController.methods.find { it.name == 'fetchAll' }.getAnnotation(RequestMapping)
        BlahController.methods.find { it.name == 'fetchOne' }.getAnnotation(ResponseBody)
        BlahController.methods.find { it.name == 'fetchOne' }.getAnnotation(RequestMapping)
        BlahController.methods.find { it.name == 'create' }.getAnnotation(ResponseBody)
        BlahController.methods.find { it.name == 'create' }.getAnnotation(RequestMapping)
        BlahController.methods.find { it.name == 'update' }.getAnnotation(ResponseBody)
        BlahController.methods.find { it.name == 'update' }.getAnnotation(RequestMapping)
        !BlahController.methods.find { it.name == 'delete' }.getAnnotation(ResponseBody)
        BlahController.methods.find { it.name == 'delete' }.getAnnotation(RequestMapping)
    }

    def 'has appropriate fields'() {
        expect:
        BlahController.fields.collect { Field field ->
            field.name
        }.containsAll(['blahService'])
    }

    def 'has appropriate methods'() {
        expect:
        BlahController.methods.collect { Method method ->
            method.name
        }.containsAll(['fetchAll', 'fetchOne', 'create', 'update', 'delete'])
    }

    def 'fetchAll'() {
        setup:
        List<Blah> blahs = [new Blah(), new Blah()]

        when:
        List<Blah> result = blahController.fetchAll()

        then:
        result == blahs
        1 * blahController.blahService.fetchAll() >> blahs
    }

    def 'fetchOne'() {
        setup:
        Blah blah = new Blah()

        when:
        Blah result = blahController.fetchOne("some string")

        then:
        result == blah
        1 * blahController.blahService.fetchOne("some string") >> blah
    }

    def 'create'() {
        setup:
        Blah blah = new Blah()

        when:
        Blah result = blahController.create(blah)

        then:
        result == blah
        1 * blahController.blahService.create(blah) >> blah
    }

    def 'update'() {
        setup:
        Blah blah = new Blah()

        when:
        Blah result = blahController.update(blah)

        then:
        result == blah
        1 * blahController.blahService.update(blah) >> blah
    }

    def 'delete'() {
        when:
        blahController.delete("some id")

        then:
        1 * blahController.blahService.delete("some id")
    }
}