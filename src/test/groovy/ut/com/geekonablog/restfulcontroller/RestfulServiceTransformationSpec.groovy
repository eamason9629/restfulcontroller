package ut.com.geekonablog.restfulcontroller

import com.geekonablog.restfulcontroller.RestfulService
import com.geekonablog.restfulcontroller.controller.BlahController
import com.geekonablog.restfulcontroller.domain.Blah
import com.geekonablog.restfulcontroller.repository.BlahRepository
import com.geekonablog.restfulcontroller.service.BlahService
import groovy.text.GStringTemplateEngine
import groovy.text.Template
import org.springframework.stereotype.Service
import spock.lang.Ignore
import spock.lang.Specification

import java.lang.reflect.Field
import java.lang.reflect.Method

class RestfulServiceTransformationSpec extends Specification {
    BlahService blahService = new BlahService(blahRepository: Mock(BlahRepository))

    @Ignore
    def 'stop point'() {
        setup:
        File physicalFile = new File(new File('').canonicalFile.toString() + '/src/test/groovy/' + BlahController.name.replace('.', '/') + '.groovy')
        final Template template = new GStringTemplateEngine().createTemplate(physicalFile?.text)
        def instance = new GroovyClassLoader().parseClass(template.make([:]).toString()).newInstance()

        expect:
        instance
    }

    def 'has appropriate annotations'() {
        expect:
        BlahService.getAnnotation(Service)
        BlahService.getAnnotation(RestfulService)
        BlahService.getAnnotation(RestfulService).domain() == Blah
        BlahService.getAnnotation(RestfulService).repository() == BlahRepository
    }

    def 'has appropriate fields'() {
        expect:
        BlahService.fields.collect { Field field ->
            field.name
        }.containsAll(['blahRepository'])
    }

    def 'has appropriate methods'() {
        expect:
        BlahService.methods.collect { Method method ->
            method.name
        }.containsAll(['fetchAll', 'fetchOne', 'create', 'update', 'delete'])
    }

    def 'fetchAll'() {
        setup:
        List<Blah> blahs = [new Blah(), new Blah()]

        when:
        List<Blah> result = blahService.fetchAll()

        then:
        result == blahs
        1 * blahService.blahRepository.fetchAll() >> blahs
    }

    def 'fetchOne'() {
        setup:
        Blah blah = new Blah()

        when:
        Blah result = blahService.fetchOne("some string")

        then:
        result == blah
        1 * blahService.blahRepository.fetchOne("some string") >> blah
    }

    def 'create'() {
        setup:
        Blah blah = new Blah()

        when:
        Blah result = blahService.create(blah)

        then:
        result == blah
        1 * blahService.blahRepository.create(blah) >> blah
    }

    def 'update'() {
        setup:
        Blah blah = new Blah()

        when:
        Blah result = blahService.update(blah)

        then:
        result == blah
        1 * blahService.blahRepository.update(blah) >> blah
    }

    def 'delete'() {
        when:
        blahService.delete("some id")

        then:
        1 * blahService.blahRepository.delete("some id")
    }
}