package ut.com.geekonablog.util

import com.geekonablog.util.StringUtils
import spock.lang.Specification
import spock.lang.Unroll

class StringUtilsSpec extends Specification {
    @Unroll
    def "lower first - #given -> #expected"() {
        expect:
        StringUtils.lowerFirst(given) == expected

        where:
        given         | expected
        null          | null
        ''            | ''
        'first'       | 'first'
        'First'       | 'first'
        ' First'      | ' First'
        'firstSecond' | 'firstSecond'
        'FirstSecond' | 'firstSecond'
    }
}
