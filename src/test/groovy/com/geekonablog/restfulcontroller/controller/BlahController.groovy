package com.geekonablog.restfulcontroller.controller

import com.geekonablog.restfulcontroller.CrudOperations
import com.geekonablog.restfulcontroller.RestfulController
import com.geekonablog.restfulcontroller.domain.Blah
import com.geekonablog.restfulcontroller.service.BlahService

@RestfulController(domain = Blah, service = BlahService)//, operations = [CrudOperations.fetchAll, CrudOperations.fetchOne, CrudOperations.create, CrudOperations.update, CrudOperations.delete])
class BlahController {}