package com.geekonablog.restfulcontroller.domain

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class Blah {
    String id = UUID.randomUUID()
}