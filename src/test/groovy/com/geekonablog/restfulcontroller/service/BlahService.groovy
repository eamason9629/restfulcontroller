package com.geekonablog.restfulcontroller.service

import com.geekonablog.restfulcontroller.RestfulService
import com.geekonablog.restfulcontroller.domain.Blah
import com.geekonablog.restfulcontroller.repository.BlahRepository

@RestfulService(domain = Blah, repository = BlahRepository)
class BlahService {}