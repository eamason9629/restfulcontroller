package com.geekonablog.restfulcontroller.repository

import com.geekonablog.restfulcontroller.CrudRepository
import com.geekonablog.restfulcontroller.domain.Blah

class BlahRepository implements CrudRepository<Blah> {
    @Override
    List<Blah> fetchAll() {
        return null
    }

    @Override
    Blah fetchOne(String id) {
        return null
    }

    @Override
    Blah create(Blah item) {
        return null
    }

    @Override
    Blah update(Blah item) {
        return null
    }

    @Override
    void delete(String id) {

    }
}
