package com.geekonablog.util

class StringUtils {
    static String lowerFirst(String given) {
        given ? given.substring(0, 1).toLowerCase() + given.substring(1) : given
    }
}
