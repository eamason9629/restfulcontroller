package com.geekonablog.restfulcontroller

import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.AnnotationNode
import org.codehaus.groovy.ast.ClassHelper
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.ast.FieldNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.Parameter
import org.codehaus.groovy.ast.expr.ArgumentListExpression
import org.codehaus.groovy.ast.expr.ClassExpression
import org.codehaus.groovy.ast.expr.ConstantExpression
import org.codehaus.groovy.ast.expr.EmptyExpression
import org.codehaus.groovy.ast.expr.ListExpression
import org.codehaus.groovy.ast.expr.MethodCallExpression
import org.codehaus.groovy.ast.expr.PropertyExpression
import org.codehaus.groovy.ast.expr.VariableExpression
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.ReturnStatement
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.AbstractASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

import static com.geekonablog.util.StringUtils.lowerFirst
import static java.lang.reflect.Modifier.PUBLIC
import static org.codehaus.groovy.control.CompilePhase.SEMANTIC_ANALYSIS
import static org.springframework.web.bind.annotation.RequestMethod.*

@GroovyASTTransformation(phase = SEMANTIC_ANALYSIS)
class RestfulControllerTransformation extends AbstractASTTransformation {
    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        AnnotationNode annotation = nodes[0] as AnnotationNode
        ClassNode controller = nodes[1] as ClassNode

        ClassNode domain = annotation.getMember('domain').type
        ClassNode service = annotation.getMember('service').type
        ListExpression operations = annotation.getMember('operations') ?: new ListExpression(CrudOperations.collect{new PropertyExpression(new ClassExpression(new ClassNode(CrudOperations)), new ConstantExpression(it.name()))})
        addImports(source)
        addAnnotations(controller, domain)
        addAutowiredFields(source, controller, [service])
        operations.expressions.each {
            controller.methods.add("${it.property?.value}"(service, domain))
        }
    }

    String className(ClassNode clazz) {
        return lowerFirst(clazz.name.split(/\./).last())
    }

    /**
     * import org.springframework.beans.factory.annotation.Autowired
     * import org.springframework.web.bind.annotation.RequestBody
     * import org.springframework.web.bind.annotation.RestController
     * import org.springframework.web.bind.annotation.RequestMapping
     *
     * class Foo {}
     */
    void addImports(SourceUnit source) {
        source.AST.addImport(Autowired.simpleName, new ClassNode(Autowired))
        source.AST.addImport(RequestBody.simpleName, new ClassNode(RequestBody))
        source.AST.addImport(RestController.simpleName, new ClassNode(RestController))
        source.AST.addImport(RequestMapping.simpleName, new ClassNode(RequestMapping))
    }

    /**
     * @RestController
     * @RequestMapping(value = "domain")
     * class Foo {}
     */
    void addAnnotations(ClassNode controller, ClassNode domain) {
        controller.addAnnotation(new AnnotationNode(new ClassNode(RestController)))
        AnnotationNode requestMapping = new AnnotationNode(new ClassNode(RequestMapping))
        requestMapping.addMember('value', new ConstantExpression(className(domain)))
        controller.addAnnotation(requestMapping)
    }

    /**
     * import "Service"
     * import "Domain"
     * import "etc"
     *
     * class Foo {
     *      @Autowired
     *      "Service" "service"
     *
     *      @Autowired
     *      "Domain" "domain"
     *
     *      @Autowired
     *      "Etc" "etc"
     * }
     */
    void addAutowiredFields(SourceUnit source, ClassNode controller, List<ClassNode> fields) {
        fields.each {
            String clazz = className(it)
            if (!source.AST.imports.alias.contains(clazz)) {
                source.AST.addImport(clazz, it)
            }
            if (!controller.fields.name.contains(clazz)) {
                FieldNode fieldNode = new FieldNode(clazz, ACC_PUBLIC, it, controller, new EmptyExpression())
                fieldNode.addAnnotation(new AnnotationNode(new ClassNode(Autowired)))
                controller.addField(fieldNode)
            }
        }
    }

    /**
     * @RequestMapping(method = GET)
     * public @ResponseBody List<"Domain"> fetchAll() {
     *     return "Service".fetchAll()
     * }
     */
    MethodNode fetchAll(ClassNode service, ClassNode domain) {
        // TODO, can new ClassNode(List) be new ClassNode(List<"Domain">)
        MethodNode methodNode = new MethodNode(
            'fetchAll', PUBLIC, new ClassNode(List), new Parameter[0], new ClassNode[0],
            new ReturnStatement(new MethodCallExpression(new VariableExpression(className(service)), 'fetchAll', new ArgumentListExpression()))
        )
        methodNode.addAnnotation(new AnnotationNode(new ClassNode(ResponseBody)))
        AnnotationNode requestMapping = new AnnotationNode(new ClassNode(RequestMapping))
        requestMapping.addMember('method', new PropertyExpression(new ClassExpression(new ClassNode(RequestMethod)), new ConstantExpression(GET)))
        methodNode.addAnnotation(requestMapping)
        return methodNode
    }

    /**
     * @RequestMapping(method = GET, value = "{id}")
     * public @ResponseBody "Domain" fetchOne(@PathVariable String id) {
     *     return "Service".fetchOne(id)
     * }
     */
    MethodNode fetchOne(ClassNode service, ClassNode domain) {
        Parameter[] parameters = [new Parameter(new ClassNode(String), 'id')] as Parameter[]
        parameters.first().addAnnotation(new AnnotationNode(new ClassNode(PathVariable)))
        MethodNode methodNode = new MethodNode('fetchOne', PUBLIC, domain, parameters, new ClassNode[0],
            new ReturnStatement(new MethodCallExpression(new VariableExpression(className(service)), 'fetchOne', new ArgumentListExpression(parameters)))
        )
        methodNode.addAnnotation(new AnnotationNode(new ClassNode(ResponseBody)))
        AnnotationNode requestMapping = new AnnotationNode(new ClassNode(RequestMapping))
        requestMapping.addMember('method', new PropertyExpression(new ClassExpression(new ClassNode(RequestMethod)), new ConstantExpression(GET)))
        requestMapping.addMember('value', new ConstantExpression('{id}'))
        methodNode.addAnnotation(requestMapping)
        return methodNode
    }

    /**
     * @RequestMapping(method = POST)
     * public @ResponseBody "Domain" create(@RequestBody "Domain" item) {
     *     return "Service".create(item)
     * }
     */
    MethodNode create(ClassNode service, ClassNode domain) {
        Parameter[] parameters = [new Parameter(domain, 'item')] as Parameter[]
        parameters.first().addAnnotation(new AnnotationNode(new ClassNode(RequestBody)))
        MethodNode methodNode = new MethodNode('create', PUBLIC, domain, parameters, new ClassNode[0],
            new ReturnStatement(new MethodCallExpression(new VariableExpression(className(service)), 'create', new ArgumentListExpression(parameters)))
        )
        methodNode.addAnnotation(new AnnotationNode(new ClassNode(ResponseBody)))
        AnnotationNode requestMapping = new AnnotationNode(new ClassNode(RequestMapping))
        requestMapping.addMember('method', new PropertyExpression(new ClassExpression(new ClassNode(RequestMethod)), new ConstantExpression(POST)))
        methodNode.addAnnotation(requestMapping)
        return methodNode
    }

    /**
     * @RequestMapping(method = PUT, value = "{id}")
     * public @ResponseBody "Domain" update(@RequestBody "Domain" item) {
     *     return "Service".update(item)
     * }
     */
    MethodNode update(ClassNode service, ClassNode domain) {
        Parameter[] parameters = [new Parameter(domain, 'item')] as Parameter[]
        parameters.first().addAnnotation(new AnnotationNode(new ClassNode(RequestBody)))
        MethodNode methodNode = new MethodNode('update', PUBLIC, domain, parameters, new ClassNode[0],
            new ReturnStatement(new MethodCallExpression(new VariableExpression(className(service)), 'update', new ArgumentListExpression(parameters)))
        )
        methodNode.addAnnotation(new AnnotationNode(new ClassNode(ResponseBody)))
        AnnotationNode requestMapping = new AnnotationNode(new ClassNode(RequestMapping))
        requestMapping.addMember('method', new PropertyExpression(new ClassExpression(new ClassNode(RequestMethod)), new ConstantExpression(POST)))
        requestMapping.addMember('value', new ConstantExpression('{id}'))
        methodNode.addAnnotation(requestMapping)
        return methodNode
    }

    /**
     * @RequestMapping(method = DELETE, value = "{id}")
     * public void delete(@RequestParam String id) {
     *     "Service".delete()
     * }
     */
    MethodNode delete(ClassNode service, ClassNode domain) {
        Parameter[] parameters = [new Parameter(new ClassNode(String), 'id')] as Parameter[]
        parameters.first().addAnnotation(new AnnotationNode(new ClassNode(PathVariable)))
        MethodNode methodNode = new MethodNode('delete', PUBLIC, ClassHelper.VOID_TYPE, parameters, new ClassNode[0],
            new ExpressionStatement(new MethodCallExpression(new VariableExpression(className(service)), 'delete', new ArgumentListExpression(parameters)))
        )
        AnnotationNode requestMapping = new AnnotationNode(new ClassNode(RequestMapping))
        requestMapping.addMember('method', new PropertyExpression(new ClassExpression(new ClassNode(RequestMethod)), new ConstantExpression(DELETE)))
        requestMapping.addMember('value', new ConstantExpression('{id}'))
        methodNode.addAnnotation(requestMapping)
        return methodNode
    }
}