package com.geekonablog.restfulcontroller

import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.expr.*
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.ReturnStatement
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.AbstractASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.*

import static com.geekonablog.util.StringUtils.lowerFirst
import static java.lang.reflect.Modifier.PUBLIC
import static org.codehaus.groovy.control.CompilePhase.SEMANTIC_ANALYSIS
import static org.springframework.web.bind.annotation.RequestMethod.*

@GroovyASTTransformation(phase = SEMANTIC_ANALYSIS)
class RestfulServiceTransformation extends AbstractASTTransformation {
    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        AnnotationNode annotation = nodes[0] as AnnotationNode
        ClassNode service = nodes[1] as ClassNode

        ClassNode domain = annotation.getMember('domain').type
        ClassNode repository = annotation.getMember('repository').type
        ListExpression operations = annotation.getMember('operations') ?: new ListExpression(CrudOperations.collect{new PropertyExpression(new ClassExpression(new ClassNode(CrudOperations)), new ConstantExpression(it.name()))})
        addImports(source)
        addAnnotations(service, domain)
        addAutowiredFields(source, service, [repository])
        operations.expressions.each {
            service.methods.add("${it.property?.value}"(repository, domain))
        }
    }

    String className(ClassNode clazz) {
        return lowerFirst(clazz.name.split(/\./).last())
    }

    /**
     * import org.springframework.beans.factory.annotation.Autowired
     * import org.springframework.stereotype.Service
     *
     * class Foo {}
     */
    void addImports(SourceUnit source) {
        source.AST.addImport(Autowired.simpleName, new ClassNode(Autowired))
        source.AST.addImport(Service.simpleName, new ClassNode(Service))
    }

    /**
     * @Service
     * class Foo {}
     */
    void addAnnotations(ClassNode controller, ClassNode domain) {
        controller.addAnnotation(new AnnotationNode(new ClassNode(Service)))
    }

    /**
     * import "Repository"
     * import "Domain"
     * import "etc"
     *
     * class Foo {
     *      @Autowired
     *      "Repository" "repository"
     *
     *      @Autowired
     *      "Domain" "domain"
     *
     *      @Autowired
     *      "Etc" "etc"
     * }
     */
    void addAutowiredFields(SourceUnit source, ClassNode service, List<ClassNode> fields) {
        fields.each {
            String clazz = className(it)
            if (!source.AST.imports.alias.contains(clazz)) {
                source.AST.addImport(clazz, it)
            }
            if (!service.fields.name.contains(clazz)) {
                FieldNode fieldNode = new FieldNode(clazz, ACC_PUBLIC, it, service, new EmptyExpression())
                fieldNode.addAnnotation(new AnnotationNode(new ClassNode(Autowired)))
                service.addField(fieldNode)
            }
        }
    }

    /**
     * public List<"Domain"> fetchAll() {
     *     return "Repository".fetchAll()
     * }
     */
    MethodNode fetchAll(ClassNode repository, ClassNode domain) {
        // TODO, can new ClassNode(List) be new ClassNode(List<"Domain">)
        MethodNode methodNode = new MethodNode('fetchAll', PUBLIC, new ClassNode(List), new Parameter[0], new ClassNode[0],
            new ReturnStatement(new MethodCallExpression(new VariableExpression(className(repository)), 'fetchAll', new ArgumentListExpression()))
        )
        return methodNode
    }

    /**
     * public "Domain" fetchOne(String id) {
     *     return "Repository".fetchOne(id)
     * }
     */
    MethodNode fetchOne(ClassNode repository, ClassNode domain) {
        Parameter[] parameters = [new Parameter(new ClassNode(String), 'id')] as Parameter[]
        MethodNode methodNode = new MethodNode('fetchOne', PUBLIC, domain, parameters, new ClassNode[0],
            new ReturnStatement(new MethodCallExpression(new VariableExpression(className(repository)), 'fetchOne', new ArgumentListExpression(parameters)))
        )
        return methodNode
    }

    /**
     * public "Domain" create("Domain" item) {
     *     return "Repository".create(item)
     * }
     */
    MethodNode create(ClassNode repository, ClassNode domain) {
        Parameter[] parameters = [new Parameter(domain, 'item')] as Parameter[]
        MethodNode methodNode = new MethodNode('create', PUBLIC, domain, parameters, new ClassNode[0],
            new ReturnStatement(new MethodCallExpression(new VariableExpression(className(repository)), 'create', new ArgumentListExpression(parameters)))
        )
        return methodNode
    }

    /**
     * public "Domain" update("Domain" item) {
     *     return "Repository".update(item)
     * }
     */
    MethodNode update(ClassNode repository, ClassNode domain) {
        Parameter[] parameters = [new Parameter(domain, 'item')] as Parameter[]
        MethodNode methodNode = new MethodNode('update', PUBLIC, domain, parameters, new ClassNode[0],
            new ReturnStatement(new MethodCallExpression(new VariableExpression(className(repository)), 'update', new ArgumentListExpression(parameters)))
        )
        return methodNode
    }

    /**
     * public void delete(String id) {
     *     "Repository".delete()
     * }
     */
    MethodNode delete(ClassNode repository, ClassNode domain) {
        Parameter[] parameters = [new Parameter(new ClassNode(String), 'id')] as Parameter[]
        MethodNode methodNode = new MethodNode('delete', PUBLIC, ClassHelper.VOID_TYPE, parameters, new ClassNode[0],
            new ExpressionStatement(new MethodCallExpression(new VariableExpression(className(repository)), 'delete', new ArgumentListExpression(parameters)))
        )
        return methodNode
    }
}