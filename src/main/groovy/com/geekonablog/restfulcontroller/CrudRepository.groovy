package com.geekonablog.restfulcontroller

interface CrudRepository<T> {
    List<T> fetchAll()
    T fetchOne(String id)
    T create(T item)
    T update(T item)
    void delete(String id)
}