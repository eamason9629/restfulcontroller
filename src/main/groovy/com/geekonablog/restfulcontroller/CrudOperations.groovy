package com.geekonablog.restfulcontroller

enum CrudOperations {
    fetchAll, fetchOne, create, update, delete
}