package com.geekonablog.restfulcontroller

import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

import static com.geekonablog.restfulcontroller.CrudOperations.*

@Target([ElementType.TYPE])
@Retention(RetentionPolicy.RUNTIME)
@GroovyASTTransformationClass('com.geekonablog.restfulcontroller.RestfulServiceTransformation')
@interface RestfulService {
    Class domain()

    Class<CrudRepository> repository()

    CrudOperations[] operations() default [fetchAll, fetchOne, create, update, delete]
}